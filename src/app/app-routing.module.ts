import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'administracion',
    loadChildren: () =>
      import('./modules/adm/adm.module').then(
        (m) => m.AdmModule
      ),
  },
  {
    path: '**',
    redirectTo: 'administracion',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

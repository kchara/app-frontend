import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { UserFilterReq, Usuario, UsuarioActualizacionReq, UsuarioCreacionReq } from '../interfaces/users.interface';
import { environment } from 'src/environments/environment';
import { CommunicationConstants } from 'src/app/core/constants/communication';
import { Departamento } from '../interfaces/departamento';
import { Cargo } from '../interfaces/cargo';



const API_MAIN = environment.serverUrl;


@Injectable({
  providedIn: 'root'
})
export class QueryManagementService {

  private url_usuarios: string = API_MAIN + 'usuarios';
  private url_departamentos: string = API_MAIN + 'departamentos';
  private url_cargos: string = API_MAIN + 'cargos';
  
  constructor(private _http: HttpClient) {
  }

  getUsuariosAll(req:UserFilterReq): Observable<any> {
    const params = new HttpParams()
    .set('idDepartamento',req.idDepartamento ?? '')
    .set('idCargo', req.idCargo ?? '')
    ;
    
    return this._http
      .get<Array<Usuario>>(this.url_usuarios,{params})
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

  getDepartamentosAll(): Observable<any> {
    return this._http
      .get<Array<Departamento>>(this.url_departamentos)
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

  getCargosAll(): Observable<any> {
    return this._http
      .get<Array<Cargo>>(this.url_cargos)
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

  createUsuario(request: UsuarioCreacionReq): Observable<any> {
    return this._http
      .post<Usuario>(this.url_usuarios, request)
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

  actualizarUsuario(id:number, request: UsuarioActualizacionReq): Observable<any> { 
    const url_update_users = `${this.url_usuarios}/${id}`;
    return this._http
      .put<Usuario>(url_update_users, request)
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

  removerUsuario(id:number): Observable<any> { 
    const url_update_users = `${this.url_usuarios}/${id}`;
    return this._http
      .delete<Usuario>(url_update_users)
      .pipe(
        map((res) => {
          const [codigosCom, mensajesCom] = CommunicationConstants;
          if (res === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          let resTmp = res;
          if (resTmp === null) {
            throw new Error(
              `${codigosCom.ERRORSRV}||${mensajesCom.MSGDATAVACIOSRV}`
            );
          }
          return resTmp;
        })
      );
  }

}

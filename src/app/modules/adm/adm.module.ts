import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmRoutingModule } from './adm-routing.module';
import { AdmIndexComponent } from './pages/adm-index/adm-index.component';
import { AdmListComponent } from './pages/adm-list/adm-list.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { AdmDialogComponent } from './pages/adm-dialog/adm-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdmHeaderComponent } from './pages/adm-header/adm-header.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  declarations: [
    AdmIndexComponent,
    AdmListComponent,
    AdmDialogComponent,
    AdmHeaderComponent,
    ConfirmationDialogComponent,
    SpinnerComponent,
  ],
  imports: [
    CommonModule,
    AdmRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ]
})
export class AdmModule { }

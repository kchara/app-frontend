import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmIndexComponent } from './pages/adm-index/adm-index.component';

const routes: Routes = [
  {path:'',component:AdmIndexComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmRoutingModule { }

export interface Usuario {
    id: number,
    usuario: string,
    primerNombre: string,
    segundoNombre: string,
    primerApellido: string,
    segundoApellido: string,
    idDepartamento: number, 
    idCargo: number,
    nombreDepartamento:string,
    nombreCargo:string,
    email:string
}

export interface UserFilterReq {
    idDepartamento?:number| null,
    idCargo?:number| null
}


export interface UsuarioCreacionReq {
    usuario: string,
    primerNombre: string,
    segundoNombre: string,
    primerApellido: string,
    segundoApellido: string,
    email:string,
    idDepartamento: number, 
    idCargo: number,
}

export interface UsuarioActualizacionReq {
    id:number,
    usuario: string,
    primerNombre: string,
    segundoNombre: string,
    primerApellido: string,
    segundoApellido: string,
    email:string
    idDepartamento: number, 
    idCargo: number,
}

export interface Departamento{
    id:number,
    nombre:string
}

export interface Cargo{
    id:number,
    nombre:string
}


import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { QueryManagementService } from '../../services/query-management.service';
import { UserFilterReq, Usuario, UsuarioActualizacionReq } from '../../interfaces/users.interface';
import { MatDialog } from '@angular/material/dialog';
import { AdmDialogComponent } from '../adm-dialog/adm-dialog.component';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-adm-list',
  templateUrl: './adm-list.component.html',
  styleUrls: ['./adm-list.component.sass']
})
export class AdmListComponent implements OnInit, OnChanges{
  
  displayedColumns: string[] = [];
  dataSource:Usuario[] = []
  @Input() actualizarList : boolean = false;
  @Input() userFilterReq!: UserFilterReq;
  loading:boolean = false;


  constructor(
    private queryManagement:QueryManagementService,
    public dialog: MatDialog
  ){
    this.displayedColumns = ['usuario', 'nombres', 'apellidos', 'departamento',  'cargo', 'email','acciones'];
    this.userFilterReq = {idCargo:null,idDepartamento:null}
  }


  ngOnChanges(changes: SimpleChanges): void {
     for (const propName in changes) {
      const req:UserFilterReq = this.userFilterReq;
      if (propName === 'actualizarList') {
       
          this.getUsersAll(req);
       
      }

      if (propName === 'userFilterReq') {
        const req:UserFilterReq = this.userFilterReq;
          this.getUsersAll(req);
      }

    }
  }

  ngOnInit(): void {
    this.getUsersAll(this.userFilterReq);
  }

  getUsersAll(req:UserFilterReq){
    this.loading = true;
    this.queryManagement.getUsuariosAll(req)
    .subscribe({
      next: (res): void => {
        this.dataSource = res;
      },
      error: (error): void => {
        this.loading = false;
        console.log(error);
      },
      complete: (): void => {
        this.loading = false;
      }
    });

  }

  edit(data:Usuario){
    this.openDialog(data);
  }

  openDialog(data:Usuario){
    const dialogRef = this.dialog.open(AdmDialogComponent, {
      data: {data},
      closeOnNavigation:true,
      maxWidth:'700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.actualizarUsuario(result);
      }
    });
  }

  actualizarUsuario(request: UsuarioActualizacionReq){
    this.loading = true;
    this.queryManagement.actualizarUsuario(request.id,request)
      .subscribe({
        next: (res): void => {
        },
        error: (error): void => {
          this.loading = false;
          console.log(error);
        },
        complete: (): void => {
          this.loading = false;
          this.getUsersAll(this.userFilterReq);
        }
      });
  }

  openModalConfirmation(id:number,enterAnimationDuration: string, exitAnimationDuration: string){
    const dialogConfirmation =  this.dialog.open(ConfirmationDialogComponent, {
      enterAnimationDuration,
      exitAnimationDuration,
      position: {
        top:'4rem'
      },
      width:'25rem',
      
    });

    dialogConfirmation.afterClosed().subscribe(result => {
      if(result){
        this.deleteUser(id);      }
    });
  }

  deleteUser(id:number){
    this.loading = true;
    this.queryManagement.removerUsuario(id)
    .subscribe({
      next: (res): void => {
      },
      error: (error): void => {
        this.loading = false;
        console.log(error);
      },
      complete: (): void => {
        this.loading = false;
        this.getUsersAll(this.userFilterReq);
      }
    });
  }





}

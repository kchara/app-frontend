import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { QueryManagementService } from '../../services/query-management.service';
import { MatDialog } from '@angular/material/dialog';
import { Cargo, Departamento, UserFilterReq, Usuario, UsuarioCreacionReq } from '../../interfaces/users.interface';
import { AdmDialogComponent } from '../adm-dialog/adm-dialog.component';

@Component({
  selector: 'app-adm-header',
  templateUrl: './adm-header.component.html',
  styleUrls: ['./adm-header.component.sass']
})
export class AdmHeaderComponent implements OnInit{

  departamentos:Departamento[] = [];
  cargos:Cargo[] = [];
  loading:boolean = false;
  req:UserFilterReq = {
    idCargo:null,
    idDepartamento:null
  }

  @Output() updateListEmitter = new EventEmitter<boolean>();
  @Output() reqUserFilter = new EventEmitter<UserFilterReq>();

  usuarioFilter: UserFilterReq = {
    idDepartamento: 0,
    idCargo: 0,
  };

  constructor(
    private queryManagement:QueryManagementService,
    public dialog: MatDialog
  ){

  }



  ngOnInit(): void {
    this.getDepartamentos();
    this.getCargos();
    this.reqUserFilter.emit(this.usuarioFilter);
  }

  
  create(){
    this.openDialog()
  }

  getDepartmento($event:any){
    if ($event.isUserInput) {
      const idDepartamento:number | null = $event.source.value ?? 0;
      this.req.idDepartamento = idDepartamento;
      this.reqUserFilter.emit({idCargo:this.req.idCargo,idDepartamento:this.req.idDepartamento});
    }
  }

  
  getCargo($event:any){

    if ($event.isUserInput) {
      const idCargo:number | null = $event.source.value;
      this.req.idCargo = idCargo;
      this.reqUserFilter.emit({idCargo:this.req.idCargo,idDepartamento:this.req.idDepartamento});
    }
  }


  openDialog(){
    const dialogRef = this.dialog.open(AdmDialogComponent, {
      data: {},
      closeOnNavigation:true,
      maxWidth:'700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.guardarHeader(result);
      }
    });
  }

  guardarHeader(user:UsuarioCreacionReq){
      this.queryManagement.createUsuario(user)
      .subscribe({
        next: (res): void => {
          if(res){
            this.updateListEmitter.emit(true);
          }
        },
        error: (error): void => {
          console.log(error);
        },
        complete: (): void => {
        }
      });
    
  }

  getDepartamentos(){
    this.loading = true;
    this.queryManagement.getDepartamentosAll()
    .subscribe({
      next: (res): void => {
          this.departamentos = res;
      },
      error: (error): void => {
        console.log(error);
        this.loading = false;
      },
      complete: (): void => {
        this.loading = false;
      }
    });
  }

  getCargos(){
    this.loading = true;
    this.queryManagement.getCargosAll()
    .subscribe({
      next: (res): void => {
          this.cargos = res;
      },
      error: (error): void => {
        console.log(error);
        this.loading = false;
      },
      complete: (): void => {
        this.loading = false;
      }
    });
  }



}

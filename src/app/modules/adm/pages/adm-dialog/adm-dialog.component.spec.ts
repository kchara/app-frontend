import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDialogComponent } from './adm-dialog.component';

describe('AdmDialogComponent', () => {
  let component: AdmDialogComponent;
  let fixture: ComponentFixture<AdmDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmDialogComponent]
    });
    fixture = TestBed.createComponent(AdmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

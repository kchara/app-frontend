import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Usuario } from '../../interfaces/users.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Departamento } from '../../interfaces/departamento';
import { QueryManagementService } from '../../services/query-management.service';
import { Cargo } from '../../interfaces/cargo';

@Component({
  selector: 'app-adm-dialog',
  templateUrl: './adm-dialog.component.html',
  styleUrls: ['./adm-dialog.component.sass']
})
export class AdmDialogComponent implements OnInit{
    
  formAdministrador!:FormGroup;
  departamentos:Departamento[] = [];
  cargos:Cargo[] = [];
  titulo:string = '';
  loading:boolean = false;

    constructor(
      public dialogRef: MatDialogRef<AdmDialogComponent>,
      private queryManagement:QueryManagementService,
      @Inject(MAT_DIALOG_DATA) public data: any,
    ){
      this.buildForm();
      this.getDepartamentos();
      this.getCargos();
    }

    ngOnInit(): void {
      if(this.data.data){
        this.titulo = 'Editar usuario';
        this.pathValueToForm();
      }else{
        this.titulo = 'Registrar usuario';
      }
    }

    pathValueToForm(){
      const dataUsuario:Usuario = this.data.data;
      this.formAdministrador.patchValue({
        id:dataUsuario.id,
        usuario: dataUsuario.usuario,
        primerNombre: dataUsuario.primerNombre,
        segundoNombre: dataUsuario.segundoNombre,
        primerApellido: dataUsuario.primerApellido,
        segundoApellido: dataUsuario.segundoApellido,
        email: dataUsuario.email,
        idDepartamento: dataUsuario.idDepartamento, 
        idCargo: dataUsuario.idCargo,
      });
    }

    buildForm(){
      this.formAdministrador = new FormGroup({
        id: new FormControl(null) ,
        usuario: new FormControl(null, [Validators.required]) ,
        email: new FormControl(null, [Validators.required, Validators.email]) ,
        primerNombre: new FormControl(null, [Validators.required]) ,
        segundoNombre: new FormControl(null, [Validators.required]) ,
        primerApellido: new FormControl(null, [Validators.required]) ,
        segundoApellido: new FormControl(null, [Validators.required]) ,
        idDepartamento: new FormControl(null, [Validators.required]) , 
        idCargo: new FormControl(null, [Validators.required]),
      });
    }

    getDepartamentos(){
      this.loading = true;
      this.queryManagement.getDepartamentosAll()
      .subscribe({
        next: (res): void => {
            this.departamentos = res;
        },
        error: (error): void => {
          console.log(error);
          this.loading = false;
        },
        complete: (): void => {
          this.loading = false;
        }
      });
    }

    getCargos(){
      this.loading = true;
      this.queryManagement.getCargosAll()
      .subscribe({
        next: (res): void => {
            this.cargos = res;
        },
        error: (error): void => {
          console.log(error);
          this.loading = false;
        },
        complete: (): void => {
          this.loading = false;
        }
      });
    }


    update(){
      
      this.dialogRef.close(this.formAdministrador?.value);
    }

    create(){
      if(this.formAdministrador.valid){
        this.dialogRef.close(this.formAdministrador?.value);
      }
    }

    close(){
      this.dialogRef.close();
    }
    
}

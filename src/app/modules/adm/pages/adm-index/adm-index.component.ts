import { Component } from '@angular/core';
import { UserFilterReq } from '../../interfaces/users.interface';

@Component({
  selector: 'app-adm-index',
  templateUrl: './adm-index.component.html',
  styleUrls: ['./adm-index.component.sass']
})
export class AdmIndexComponent {

  actualizarList:boolean = false;
  userFilterReq!:UserFilterReq;

  verifyIsUpdateListEmitter(event:boolean){
    this.actualizarList = event;
  }

  getFilterUserRequest(event:UserFilterReq){
    this.userFilterReq = event;
  }
}
